var btn_sum = document.getElementById("btn_sum")
var btn_rest = document.getElementById("btn_rest")
var btn_div = document.getElementById("btn_div")
var btn_mult = document.getElementById("btn_mult")

var result = document.getElementById("result")

var inp_1 = document.getElementById("input_1")
var inp_2 = document.getElementById("input_2")

var alerta = document.getElementById("alerta_error")


btn_sum.addEventListener("click", () => {
    var n1 = inp_1.value
    var n2 = inp_2.value
    result.innerHTML = sum(n1, n2)

})

btn_rest.addEventListener("click", () => {
    var n1 = inp_1.value
    var n2 = inp_2.value
    
    result.innerHTML = res(n1, n2)

})

btn_div.addEventListener("click", () => {
    var n1 = inp_1.value
    var n2 = inp_2.value

    result.innerHTML = div(n1, n2)

})

btn_mult.addEventListener("click", () => {
    var n1 = inp_1.value
    var n2 = inp_2.value
    
    result.innerHTML = mult(n1, n2)

})

sum = (n1, n2) => {
    validate(n1,n2)
    return parseInt(n1) + parseInt(n2)
}

res = (n1, n2) => {
    validate(n1,n2)
    return parseInt(n1) - parseInt(n2)
}

div = (n1, n2) => {
   
    validate(n1,n2)

    if (n2 == '0') {
        $('#alerta_division').removeClass('hide').addClass('show')
        return result.textContent
    } else {
        $('#alerta_division').removeClass('show').addClass('hide')
    }

    return parseInt(n1)/parseInt(n2)
}

mult = (n1, n2) => {
    validate(n1,n2)
    return parseInt(n1)*parseInt(n2)
}

validate = (n1, n2) => {

    if (n1 == "" || n2 == "") {
        $('#alerta_no_numeros').removeClass('hide').addClass('show')
        return

    } else {
        $('#alerta_no_numeros').removeClass('show').addClass('hide')
    }

}

